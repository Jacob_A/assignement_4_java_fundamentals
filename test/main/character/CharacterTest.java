package main.character;

import main.equipment.*;
import main.util.*;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    // First nine test are inspired (and in fact copied) from the assignment description
    // Last two test are checks to make sure things beyond the assignment
    // description test scope is tested as well
    @Test
    public void characterEquipment_equipWeaponWithTooHighLevel_throwInvalidWeaponError() {

        Warrior testWarrior = new Warrior("Bob");

        Weapon testAxeWeapon = new Weapon();
        testAxeWeapon.setName("Common axe");
        testAxeWeapon.setLevel(2);
        testAxeWeapon.setSlot(Slot.WEAPON);
        testAxeWeapon.setType(WeaponTypes.AXE);
        testAxeWeapon.setDamage(7);
        testAxeWeapon.setAttackSpeed(1.1);

        assertThrows(Throwable.class, () -> testWarrior.equipWeapon(testAxeWeapon));
    }

    @Test
    public void characterEquipment_equipArmorWithTooHighLevel_throwInvalidArmorError() {

        Warrior testWarrior = new Warrior("Bob");

        Armor testPlateArmor = new Armor();
        testPlateArmor.setName("Common Plate Body Armor");
        testPlateArmor.setLevel(2);
        testPlateArmor.setSlot(Slot.BODY);
        testPlateArmor.setType(ArmorTypes.PLATE);
        testPlateArmor.setAttributes(new Attributes(1, 0, 0));

        assertThrows(Throwable.class, () -> testWarrior.equipArmor(testPlateArmor));
    }

    @Test
    public void characterEquipment_equipWeaponWithWrongType_throwInvalidWeaponError() {

        Warrior testWarrior = new Warrior("Bob");

        Weapon testBowWeapon = new Weapon();
        testBowWeapon.setName("Common bow");
        testBowWeapon.setLevel(1);
        testBowWeapon.setSlot(Slot.WEAPON);
        testBowWeapon.setType(WeaponTypes.BOW);
        testBowWeapon.setDamage(7);
        testBowWeapon.setAttackSpeed(1.1);

        assertThrows(Throwable.class, () -> testWarrior.equipWeapon(testBowWeapon));
    }

    @Test
    public void characterEquipment_equipArmorWithWrongType_throwInvalidArmorError() {

        Warrior testWarrior = new Warrior("Bob");

        Armor testClothArmor = new Armor();
        testClothArmor.setName("Common Cloth Head Armor");
        testClothArmor.setLevel(2);
        testClothArmor.setSlot(Slot.HEAD);
        testClothArmor.setType(ArmorTypes.CLOTH);
        testClothArmor.setAttributes(new Attributes(1, 0, 0));

        assertThrows(Throwable.class, () -> testWarrior.equipArmor(testClothArmor));
    }

    @Test
    public void characterEquipment_equipValidWeapon_returnTrueBoolean() throws EquipmentErrors.InvalidWeaponException {

        Warrior testWarrior = new Warrior("Bob");

        Weapon testWeapon = new Weapon();
        testWeapon.setName("Common axe");
        testWeapon.setLevel(1);
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setType(WeaponTypes.AXE);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);

        assertTrue(testWarrior.equipWeapon(testWeapon));
    }

    @Test
    public void characterEquipment_equipValidArmor_returnTrueBoolean() throws EquipmentErrors.InvalidArmorException {

        Warrior testWarrior = new Warrior("Bob");

        Armor testPlateArmor = new Armor();
        testPlateArmor.setName("Common Plate Body Armor");
        testPlateArmor.setLevel(1);
        testPlateArmor.setSlot(Slot.BODY);
        testPlateArmor.setType(ArmorTypes.PLATE);
        testPlateArmor.setAttributes(new Attributes(1, 0, 0));

        assertTrue(testWarrior.equipArmor(testPlateArmor));
    }

    @Test
    public void characterDps_calculateDpsWithoutWeaponOrAmor_returnCorrectDoubleValueForDps() {
        Warrior testWarrior = new Warrior("Bob");
        double expectedDps = 1.05;
        double actual = testWarrior.getCharacterDPS();

        assertEquals(expectedDps, actual);
    }

    @Test
    public void characterDps_calculateDpsWithWeaponAndWithoutAmor_returnCorrectDoubleValueForDps() throws EquipmentErrors.InvalidWeaponException {
        Warrior testWarrior = new Warrior("Bob");
        Weapon testWeapon = new Weapon();

        testWeapon.setName("Common axe");
        testWeapon.setLevel(1);
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setType(WeaponTypes.AXE);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);

        testWarrior.equipWeapon(testWeapon);

        double expectedDps = 8.085;
        double actual = testWarrior.getCharacterDPS();

        assertEquals(expectedDps, actual);
    }

    @Test
    public void characterDps_calculateDpsWithWeaponAndAmor_returnCorrectDoubleValueForDps() throws EquipmentErrors.InvalidWeaponException, EquipmentErrors.InvalidArmorException {
        Warrior testWarrior = new Warrior("Bob");
        Weapon testWeapon = new Weapon();

        testWeapon.setName("Common axe");
        testWeapon.setLevel(1);
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setType(WeaponTypes.AXE);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);

        testWarrior.equipWeapon(testWeapon);

        Armor testPlateArmor = new Armor();
        testPlateArmor.setName("Common Plate Body Armor");
        testPlateArmor.setLevel(1);
        testPlateArmor.setSlot(Slot.BODY);
        testPlateArmor.setType(ArmorTypes.PLATE);
        testPlateArmor.setAttributes(new Attributes(1, 0, 0));

        testWarrior.equipArmor(testPlateArmor);

        double expectedDps = 8.162;
        double actual = testWarrior.getCharacterDPS();

        assertEquals(expectedDps, actual);
    }

    @Test
    public void characterLevelUp_callLevelUpAndCorrectStatsAreApplied_returnCorrectValueOfAllStats() {
        Warrior testWarrior = new Warrior("Bob");
        testWarrior.levelUp();

        Attributes expectedStats = new Attributes(8, 4, 2);
        int actualInt = testWarrior.getAttribute().getAttributeInt();
        int actualDex = testWarrior.getAttribute().getAttributeDex();
        int actualStr = testWarrior.getAttribute().getAttributeStr();


        assertEquals(expectedStats.getAttributeStr(), actualStr);
        assertEquals(expectedStats.getAttributeDex(), actualDex);
        assertEquals(expectedStats.getAttributeInt(), actualInt);
    }

    @Test
    public void characterSheet_callDisplayStatAndCreateACorrectStringWithInfo_returnStringWithCorrectlyDisplayingStats() {
        Warrior testWarrior = new Warrior("Bob");

        String expected = """
                Name: Bob
                Level: 1
                Strength: 5
                Dexterity: 2
                Intelligence: 1
                DPS: 1.05
                """;
        String actual = testWarrior.displayStats();

        assertEquals(expected, actual);
    }


}