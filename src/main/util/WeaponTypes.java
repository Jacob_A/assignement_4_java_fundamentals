package main.util;

// Enum for each of the weapon type
// Null is used during initialization
// to enable checking if "empty" weapon is being
// added to any character
public enum WeaponTypes {
    AXE,
    BOW,
    DAGGER,
    HAMMER,
    STAFF,
    SWORD,
    WAND,
    NULL
}
