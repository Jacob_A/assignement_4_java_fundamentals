package main.util;


// Class to hold a set of attributes for strength, dexterity and intellect
public class Attributes {

    // 0 are used as initializing
    private int[] stat = {0, 0, 0};

    public Attributes(int strStat, int dexStat, int intStat) {
        this.stat[0] = strStat;
        this.stat[1] = dexStat;
        this.stat[2] = intStat;
    }

    // Getters for the attributes.
    // Setter are not used.
    public int getAttributeStr() {
        return this.stat[0];
    }

    public int getAttributeDex() {
        return this.stat[1];
    }

    public int getAttributeInt() {
        return this.stat[2];
    }

}
