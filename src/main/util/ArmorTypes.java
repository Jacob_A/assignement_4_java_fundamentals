package main.util;

// Enum for each of the armor type
// Null is used during initialization
// to enable checking if "empty" armor is being
// added to any character
public enum ArmorTypes {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE,
    NULL
}
