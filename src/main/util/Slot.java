package main.util;

// Enum for the slots an available to be equipped
// to a character
public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}

