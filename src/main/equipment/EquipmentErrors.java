package main.equipment;

// Custom error handling if weapon or armor functions throws errors during runtime.
public class EquipmentErrors {

    public static class InvalidWeaponException extends Throwable {
        public InvalidWeaponException(String errMsg) {
            super(errMsg);
        }
    }

    public static class InvalidArmorException extends Throwable {
        public InvalidArmorException(String errMsg) {
            super(errMsg);
        }
    }
}
