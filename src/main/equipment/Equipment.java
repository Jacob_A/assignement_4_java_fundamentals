package main.equipment;

import main.util.Slot;

// Parent class for all equipment.
public abstract class Equipment {
    private String name;
    private int level;
    private Slot slot;

    // Getters and setters for values of equipment
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }
}