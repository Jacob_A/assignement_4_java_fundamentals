package main.equipment;

import main.util.Slot;
import main.util.WeaponTypes;

public class Weapon extends Equipment {


    private WeaponTypes type;
    private int damage;
    private double attackSpeed;

    // Default constructor
    public Weapon() {
        this.setName("NAME_DEFAULT_WEAPON");
        this.setLevel(0);
        this.setSlot(Slot.WEAPON);
        this.type = WeaponTypes.NULL;
        this.damage = 0;
        this.attackSpeed = 0;

    }

    // Constructor with set values from the creation of the object;
    // Assumption is made that a weapon is always of slot weapon when created
    public Weapon(String name, int level, WeaponTypes type, int damage, double attackSpeed) {
        this.setName(name);
        this.setLevel(level);
        this.setSlot(Slot.WEAPON);
        this.type = type;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }

    // Getters and setters for values of weapon
    public WeaponTypes getType() {
        return type;
    }

    public void setType(WeaponTypes type) {
        this.type = type;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public double getWeaponDPS() {
        return (this.damage * this.attackSpeed);
    }

}
