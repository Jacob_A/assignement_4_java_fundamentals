package main.equipment;

import main.util.ArmorTypes;
import main.util.Attributes;
import main.util.Slot;

public class Armor extends Equipment {

    private ArmorTypes type;

    Attributes stats;

    //Default constructor
    public Armor() {
        this.setName("NAME_DEFAULT_WEAPON");
        this.setLevel(0);
        this.setSlot(Slot.WEAPON);
        this.type = ArmorTypes.NULL;
    }

    // Constructor with set values from the creation of the object
    public Armor(String name, int level, Slot slot, ArmorTypes type, Attributes stats) {
        this.setName(name);
        this.setLevel(level);
        this.setSlot(slot);
        this.type = type;
        this.stats = stats;
    }

    // Getters and Setters for values of armour
    public ArmorTypes getType() {
        return type;
    }

    public void setType(ArmorTypes type) {
        this.type = type;
    }

    public Attributes getAttributes() {
        return stats;
    }

    public void setAttributes(Attributes stats) {
        this.stats = stats;
    }
}
