package main.character;

import main.equipment.*;
import main.util.*;

// All the functions look very similar for each of the children of character.
// Mage is fully commented, the rest will refer back to here.
public class Mage extends Character {

    // Constructor, adds the specific values for each hero type
    // "pushes" these to the super class (Character)
    public Mage(String name) {
        super(name, 1, 1, 8);
    }

    // Level up function. Sets the specific number of attributes for each hero type each time
    // the level up function is called, prints out a small text saying the
    // character leveled up
    @Override
    public void levelUp() {
        Attributes levelUpStats = new Attributes(1, 1, 5);
        this.setLevel();
        this.setAttribute(levelUpStats);
        System.out.println(getName() + " just leveled to " + getLevel());
    }

    // Equip weapon function.
    @Override
    public boolean equipWeapon(Weapon weapon) throws EquipmentErrors.InvalidWeaponException {
        // Check if the character is eligible to equip a certain type. Specific for each hero type
        if (weapon.getType() == WeaponTypes.STAFF || weapon.getType() == WeaponTypes.WAND) {
            // Check if the character has high enough level to equip the weapon
            if (weapon.getLevel() <= getLevel()) {
                // Adds the item to the map of items for the character
                setEquippedItems(weapon, weapon.getSlot());
                return true;
            }
            // If the level is too low, throws error too low level
            else {
                throw new EquipmentErrors.InvalidWeaponException("Error: Level too low for weapon");
            }
        }
        // If type is wrong, throws error for wrong weapon type
        else {
            throw new EquipmentErrors.InvalidWeaponException("Can not equip weapon of type: " + weapon.getType());
        }
    }

    // Equip armor function.
    @Override
    public boolean equipArmor(Armor armor) throws EquipmentErrors.InvalidArmorException {
        // Check if the character is eligible to equip a certain type. Specific for each hero type
        if (armor.getType() == ArmorTypes.CLOTH) {
            // Check if the character has high enough level to equip the weapon
            if (armor.getLevel() <= getLevel()) {
                setEquippedItems(armor, armor.getSlot());
                return true;
            }
            // If the level is too low, throws error too low level
            else {
                throw new EquipmentErrors.InvalidArmorException("Error: Level too low for armor");
            }
        }
        // If type is wrong, throws error for wrong weapon type
        else {
            throw new EquipmentErrors.InvalidArmorException("Can not equip Armor of type: " + armor.getType());
        }
    }

    // Character dps function
    @Override
    public double getCharacterDPS() {
        // Creates a temporary holder for the primary attribute for each hero type and
        // adds all equipped values to that primary attribute value for
        // calculation of dps
        int primaryInt = getAttribute().getAttributeInt();
        // All functions are skipped if the getEquipped-function returns null
        // i.e. no item is equipped in that slot
        if (getEquippedItems(Slot.HEAD) != null) {
            Armor tempHead = (Armor) getEquippedItems(Slot.HEAD);
            primaryInt += tempHead.getAttributes().getAttributeInt();

        }
        if (getEquippedItems(Slot.BODY) != null) {
            Armor tempBody = (Armor) getEquippedItems(Slot.BODY);
            primaryInt += tempBody.getAttributes().getAttributeInt();
        }
        if (getEquippedItems(Slot.LEGS) != null) {
            Armor tempLegs = (Armor) getEquippedItems(Slot.LEGS);
            primaryInt += tempLegs.getAttributes().getAttributeInt();
        }
        // Calls the dps calculation from the character class with the
        // specific primary attribute
        return calcDps(primaryInt);
    }


}