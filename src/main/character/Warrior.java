package main.character;

import main.equipment.Armor;
import main.equipment.EquipmentErrors;
import main.equipment.Weapon;
import main.util.ArmorTypes;
import main.util.Attributes;
import main.util.Slot;
import main.util.WeaponTypes;

// Check comments of mages for further explanation
public class Warrior extends Character {
    public Warrior(String name) {
        super(name, 5, 2, 1);
    }

    @Override
    public void levelUp() {
        Attributes levelUpStats = new Attributes(3, 2, 1);
        this.setLevel();
        this.setAttribute(levelUpStats);
        System.out.println(getName() + " just leveled to " + getLevel());
    }

    public boolean equipWeapon(Weapon weapon) throws EquipmentErrors.InvalidWeaponException {
        if (weapon.getType() == WeaponTypes.AXE || weapon.getType() == WeaponTypes.HAMMER || weapon.getType() == WeaponTypes.SWORD) {
            if (weapon.getLevel() <= getLevel()) {
                setEquippedItems(weapon, weapon.getSlot());
                return true;
            } else {
                throw new EquipmentErrors.InvalidWeaponException("Error: Level too low for weapon");
            }
        } else {
            throw new EquipmentErrors.InvalidWeaponException("Can not equip weapon of type: " + weapon.getType());
        }
    }

    @Override
    public boolean equipArmor(Armor armor) throws EquipmentErrors.InvalidArmorException {
        if (armor.getType() == ArmorTypes.MAIL || armor.getType() == ArmorTypes.PLATE) {
            if (armor.getLevel() <= getLevel()) {
                setEquippedItems(armor, armor.getSlot());
                return true;
            } else {
                throw new EquipmentErrors.InvalidArmorException("Error: Level too low for armor");
            }
        } else {
            throw new EquipmentErrors.InvalidArmorException("Can not equip weapon of type: " + armor.getType());
        }
    }

    @Override
    public double getCharacterDPS() {
        int primaryStr = getAttribute().getAttributeStr();
        if (getEquippedItems(Slot.HEAD) != null) {
            Armor tempHead = (Armor) getEquippedItems(Slot.HEAD);
            primaryStr += tempHead.getAttributes().getAttributeStr();

        }
        if (getEquippedItems(Slot.BODY) != null) {
            Armor tempBody = (Armor) getEquippedItems(Slot.BODY);
            primaryStr += tempBody.getAttributes().getAttributeStr();
        }
        if (getEquippedItems(Slot.LEGS) != null) {
            Armor tempLegs = (Armor) getEquippedItems(Slot.LEGS);
            primaryStr += tempLegs.getAttributes().getAttributeStr();
        }


        return calcDps(primaryStr);
    }
}