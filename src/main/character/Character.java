package main.character;


import main.equipment.*;
import main.util.*;

import java.util.HashMap;

abstract class Character implements CharacterFunction {
    private String name;
    private int level = 1;
    private int strStat;
    private int dexStat;
    private int intStat;

    // A map of items currently equipped to the specific character.
    // Map is used to make sure only one item is equipped in a given slot
    // as the Slot-type is used as a key-value for the map
    HashMap<Slot, Equipment> equippedSlots = new HashMap<>();

    // Initialization of the character, protected as only child classes should call this
    // and "push" them upwards from their own constructor
    protected Character(String name, int strStat, int dexStat, int intStat) {
        this.name = name;
        this.strStat = strStat;
        this.dexStat = dexStat;
        this.intStat = intStat;
    }

    // Getters and Setters for character class
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setLevel() {
        this.level++;
    }

    public int getLevel() {
        return this.level;
    }

    public void setAttribute(Attributes stats) {
        this.strStat += stats.getAttributeStr();
        this.dexStat += stats.getAttributeDex();
        this.intStat += stats.getAttributeInt();
    }

    public Attributes getAttribute() {
        return new Attributes(this.strStat, this.dexStat, this.intStat);
    }

    public void setEquippedItems(Equipment equipment, Slot slot) {
        equippedSlots.put(slot, equipment);
    }

    public Equipment getEquippedItems(Slot slot) {
        if (equippedSlots.get(slot) != null) {
            return equippedSlots.get(slot);
        }
        return null;
    }

    // Behaviours identical for each child class, reduces duplicate of code

    // Calculates the character dps with or without a weapon equipped and returns the value
    protected double calcDps(int primary) {

        Weapon temp = (Weapon) getEquippedItems(Slot.WEAPON);
        // If the character does not have a weapon equipped, the
        // function returns null and the calculation below is used
        if (temp == null)
            return (1 + ((double) primary / 100));

        // If a weapon is found the specific weapon dps is fetched and used
        // in the dps calculation
        double weaponDPS = temp.getWeaponDPS();
        return (weaponDPS * (1 + ((double) primary / 100)));
    }

    // String builder for displaying stats for a given character
    public String displayStats() {
        // Fetches the current base attributes for the character
        // These are used as base for the total number of stats when
        // eventual armor is added
        int strStatTemp = this.strStat;
        int dexStatTemp = this.dexStat;
        int intStatTemp = this.intStat;

        // All the below checks if a certain slot contains an armor item
        // if so, take each of the attribute values and add them to the total value
        // of attributes
        if (getEquippedItems(Slot.HEAD) != null) {
            Armor tempHead = (Armor) getEquippedItems(Slot.HEAD);
            strStatTemp += tempHead.getAttributes().getAttributeStr();
            dexStatTemp += tempHead.getAttributes().getAttributeDex();
            intStatTemp += tempHead.getAttributes().getAttributeInt();
        }

        if (getEquippedItems(Slot.BODY) != null) {
            Armor tempBody = (Armor) getEquippedItems(Slot.BODY);
            strStatTemp += tempBody.getAttributes().getAttributeStr();
            dexStatTemp += tempBody.getAttributes().getAttributeDex();
            intStatTemp += tempBody.getAttributes().getAttributeInt();
        }

        if (getEquippedItems(Slot.LEGS) != null) {
            Armor tempLegs = (Armor) getEquippedItems(Slot.LEGS);
            strStatTemp += tempLegs.getAttributes().getAttributeStr();
            dexStatTemp += tempLegs.getAttributes().getAttributeDex();
            intStatTemp += tempLegs.getAttributes().getAttributeInt();
        }


        // Create a string to be a printable "sheet" to display
        return "Name: " + this.name + "\n" +
                "Level: " + this.level + "\n" +
                "Strength: " + strStatTemp + "\n" +
                "Dexterity: " + dexStatTemp + "\n" +
                "Intelligence: " + intStatTemp + "\n" +
                "DPS: " + getCharacterDPS() + "\n";
    }

}

// Interface for functions implemented by the children
interface CharacterFunction {
    void levelUp();

    boolean equipWeapon(Weapon weapon) throws EquipmentErrors.InvalidWeaponException;

    boolean equipArmor(Armor armor) throws EquipmentErrors.InvalidArmorException;

    double getCharacterDPS();

}