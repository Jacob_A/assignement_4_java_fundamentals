# RPG-Character-Sheet

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Assignment project for Experis Academy, Assignment 4 Java RPGCharacters

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Contributors](#contributors)
- [Contributing](#contributing)

## Background

This is a console application that demonstrate some concepts presented in Module 4 - Java Fundamentals. 

A large focus is the design and implementation regards OOD. 

Test has been implemented to assert different implemented functions and ideas. 
Main has no current implementation other than being an empty entry point for Java. 

## Install

- Install JDK 17
- Install Intellij
- Clone repository


## Contributors

[@Jacob_A](https://gitlab.com/Jacob_A)

## Contributing

PRs are not accepted
